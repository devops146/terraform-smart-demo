# Setting tf state in S3

provider "aws" {
  region = "eu-north-1"
}


terraform {

  backend "s3" {
    bucket         = "terraform-production-workshop"
    encrypt        = true
    key            = "terraform-demo-state"
    region         = "eu-north-1"
    dynamodb_table = "lock-tf"
  }
}
