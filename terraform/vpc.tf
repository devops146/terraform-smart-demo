# We are using an online module from here: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.51.0
# VPC is the way through which you can define your Network(Virtual Private Cloud)

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.51.0"

  name = "demo-vpc-${local.env}"
  cidr = "10.0.0.0/16"

  azs             = local.availability_zone
  private_subnets = local.private_subnets
  public_subnets  = local.public_subnets

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = local.default_tags
}
