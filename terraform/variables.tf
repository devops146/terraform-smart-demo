# control variables for each enviroment we have
locals {
  # Set enviornment name equal to the "workspace" where we deploy
  env = terraform.workspace

  default_tags = {
    Terraform   = "true"
    Owner       = "DevOps"
    Environment = terraform.workspace
  }

  ec2_counts_list = {
    "default"     = 1
    "development" = 1
    "production"  = 2
  }

  instances_list = {
    "default"     = "t3.micro"
    "development" = "t3.micro"
    "production"  = "t3.small"
  }

  availability_zones_list = {
    "default"     = ["eu-west-1a"]
    "development" = ["eu-west-1a", "eu-west-1b"]
    "production"  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  }

  private_subnets_list = {
    "default"     = ["10.0.1.0/24"]
    "development" = ["10.0.1.0/24", "10.0.2.0/24"]
    "production"  = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  }

  public_subnets_list = {
    "default"     = ["10.0.51.0/24"]
    "development" = ["10.0.51.0/24", "10.0.52.0/24"]
    "production"  = ["10.0.51.0/24", "10.0.52.0/24", "10.0.53.0/24"]
  }

  # Set variables to the value for their own different environment
  count         = lookup(local.ec2_counts_list, local.env)
  instance_type = lookup(local.instances_list, local.env)

  availability_zone = lookup(local.availability_zones_list, local.env)
  private_subnets   = lookup(local.private_subnets_list, local.env)
  public_subnets    = lookup(local.public_subnets_list, local.env)
}
