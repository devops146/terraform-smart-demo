# Terraform Smart Demo

Project status:
[![pipeline status](https://gitlab.com/devops146/terraform-smart-demo/badges/master/pipeline.svg)](https://gitlab.com/devops146/terraform-smart-demo/-/commits/master)

A Terraform Demo pipeline integrated with **Google Assistant** and **IFTTT**.

## Workflow 🔎

The workflow of the way services are connected within **IFTTT** for this Demo are as it follows:

![ifttt](./images/ifttt.png)

## Contributors 🙏

Presentation and demo prepared by:

  - Caraiman Stefan 🦝
  - Ichim Gabriel 🤵
  - Panica Dana 💃
