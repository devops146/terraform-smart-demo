#!/usr/local/bin/bash


echo "Started pipeline at $(date)"
echo "Going to run: terraform ${ACTION} in ${ENVIRONMENT}"

cd terraform/
terraform init
terraform workspace select "${ENVIRONMENT}"

if [[ "$ACTION" == *"plan"* ]]; then
   terraform plan
elif [[ "$ACTION" == *"apply"* ]]; then
   terraform apply -auto-approve
elif [[ "$ACTION" == *"destroy"* ]]; then
   terraform destroy -auto-approve
else
   # Default/fallback scenario in case we don't understand the action
   terraform plan
fi

echo "Pipeline finished executing $(date)"
