FROM hashicorp/terraform:0.13.3 as terraform
FROM wata727/tflint:0.20.2 as tflint
FROM liamg/tfsec:v0.27.0 as tfsec
FROM quay.io/terraform-docs/terraform-docs:0.10.0-rc.1 as terraform-docs
FROM hadolint/hadolint:latest-debian as hadolint
FROM koalaman/shellcheck:stable as shellcheck
FROM bridgecrew/checkov:1.0.549 as checkov
FROM alpine/helm:3.3.3 as helm

FROM bash:rc

ENV TF_PLUGIN_CACHE_DIR=/opt/terraform/

COPY --from=terraform /bin/terraform /usr/bin/terraform
COPY --from=helm /usr/bin/helm /usr/bin/helm

COPY providers.tf $TF_PLUGIN_CACHE_DIR/tmp/

WORKDIR $TF_PLUGIN_CACHE_DIR/tmp/

RUN mkdir -p $TF_PLUGIN_CACHE_DIR \
    && terraform init \
    && apk add git=2.26.2-r0 --no-cache
